/* GPL blabla free burger pls
agerlib-arduino, library for sending and receving message with arduino using pagerlin and RadioHead
*/

#include <RadioHead.h>
#include <RH_ASK.h>
#include <RHDatagram.h>
#include <SPI.h> // Not actually used but needed to compile
#include "../pagerlib/pagerlib.h"
#include "../pagerlib/packets.h"
#include <pagerlib-arduino.h>

RH_ASK driver(BITRATE);
RHDatagram manager(driver, ADDRESS);

/* pl_radio_init() 
   initialize the RadioHead lib
*/
uint8_t pl_radio_init()
{
#ifdef ARDUINO
  if (!manager.init())
    return 1; //manager init failed
#else
  printf("I dont know what to do here were is the radio?\n");
  return 1;
#endif
  return 0;
}

/* pl_send_to_radio(struct pl_pagermessage *message_ptr)
   Send a message out via the RadioHead lib
*/
uint8_t pl_send_to_radio(struct pl_pagermessage *message_ptr)
{
#ifdef ARDUINO
  Serial.print("sending - ");
  manager.setHeaderId(52);
  manager.sendto((uint8_t *)message_ptr, sizeof(struct pl_pagermessage), 2); //FIXME hardcoded address which we wont use
  manager.waitPacketSent();    
  Serial.println("OK"); 
#else
  printf("I dont know what to do here were is the radio?\n");
  return 1;
#endif
  return 0;
}

/* pl_receive_from_radio(struct pl_pagermessage *message_ptr) 
   receive a message from radio lib and put it in  *message_ptr for 
*/
uint8_t pl_receive_from_radio(struct pl_pagermessage *message_ptr)                                                                                                                   
{
#ifdef ARDUINO
  uint8_t buf[RH_ASK_MAX_MESSAGE_LEN];
  uint8_t buflen = sizeof(buf);
  uint8_t from;
  uint8_t to;
  uint8_t id;   
  Serial.print("Waiting for message - ");
  manager.waitAvailable();
  if (manager.recvfrom(buf, &buflen, &from, &to ,&id))
  {
    Serial.print("got message from : ");
    Serial.print(from, DEC);
    Serial.print(" : ");
    Serial.print(id);
    Serial.println(" : ");
   
    memcpy(message_ptr, &buf, sizeof(struct pl_pagermessage)); // copy the message to the struct

  }
#else
  printf("I dont know what to do here were is the radio?\n");
  return 1;
#endif
  return 0;
}



