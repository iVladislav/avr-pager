
#ifdef __cplusplus
extern "C"
{
#endif



//#define NOCRYPT


#if defined(__AVR_ATmega328P__)
#define ARDUINO
#endif

#include "../micro-ecc/uECC.h"


#ifdef ARDUINO
 #include <AESLib.h>
 #define DBG
 #define DB
 #define DBM
#else
 #define DBG(f_, ...) printf((f_), ##__VA_ARGS__);
 #define DB(f_, ...) dump_buffer((f_), ##__VA_ARGS__);
 #define DBM(f_, ...) dump_buffer_msg((f_), ##__VA_ARGS__);


 #include "mbedtls/entropy.h"
 #include "mbedtls/ctr_drbg.h"
 #include "mbedtls/aes.h"

#endif



#include "packets.h"

/*
 * pagerlib context for sending or receiving messages
 */
struct pl_ctx
{
  // the curve we are using (maybe this should be somewhere else?)
  uECC_Curve curve;
  // place to store the calculated shared secret
  char shared_secret[SHARED_SECRET_SIZE];
  // place to store the decompressed public key
  char decompressed_point[64];
  // contains the message that will be send or is received
  char clear_message[MSG_SIZE];
  // my keypair
  struct pl_keypair *kp;
 // struct pl_keypair *to, *from;
  struct pl_pagermessage *msg;
  
  uint8_t receiver_compressed_point[ECC_COMPRESSED_SIZE];

#ifdef ARDUINO
  aes_context aes_ctx;
  //aes_context dec_aes;
#else
mbedtls_aes_context aes_ctx;
  //mbedtls_aes_context dec_aes;
#endif


};



//const struct uECC_Curve_t * curve;
// mbedtls things
//mbedtls_aes_context enc_aes;
//mbedtls_aes_context dec_aes;

struct pl_ctx *  pl_init();
int pl_create_keypair(struct pl_ctx*, struct pl_keypair *);
int pl_send_message(struct pl_ctx *);

int pl_receive_message(struct pl_ctx*);

int pl_save_key(struct pl_keypair *key, char * filename);
int pl_load_key(struct pl_keypair *key, char * filename);
 
#ifdef __cplusplus
} /* end of extern "C" */
#endif




