
#ifndef packets_h


#ifdef __cplusplus
extern "C"
{
#endif


/* curve ecc size and shared secret size
 ECC_COMPRESSED_SIZE: size of the compressed publiv key, curvesize+1, for example, if the curve is secp256r1 compressed must be 33 bytes long.
 curve can be:
   uECC_secp160r1();
   uECC_secp192r1();
   uECC_secp224r1();
   uECC_secp256k1();

*/ 
/*
#define CURVE uECC_secp192r1()
#define ECC_COMPRESSED_SIZE 25
#define SHARED_SECRET_SIZE 24
*/

#define CURVE uECC_secp256r1()
#define ECC_COMPRESSED_SIZE 33
#define SHARED_SECRET_SIZE 39

/* AES stuff
  AES_KEYSIZE: keysize of aes key
  IV_SIZE: always 16? FIXME
*/
//#define AES_KEYSIZE 192
#define AES_KEYSIZE 256
#define IV_SIZE 16


/* message stuff
  MSG_SIZE:  size of the actual message
*/
#define MSG_SIZE 32

// pager.h

/*
 * pager packet format
 */


struct pl_keypair
{
//    uint8_t public_key[64];
    uint8_t private_key[32];
    uint8_t compressed_point[ECC_COMPRESSED_SIZE];
};

  //Will be filled in with the compressed public key. Must be at least
  //                   (curve size + 1) bytes long; for example, if the curve is secp256r1,
  //                   compressed must be 33 bytes long.

struct pl_pagermessage 
{
  uint8_t sender_compressed_point[ECC_COMPRESSED_SIZE];
  unsigned char iv[IV_SIZE];
  char msg[MSG_SIZE];
};

/* subjectPublicKeyInfo field
 *    in X.509 certificates [PKI]
 * https://www.ietf.org/rfc/rfc5480.txt
 * https://tools.ietf.org/html/rfc5915
 */

/*
 * gpg
 *   A V4 fingerprint is the 160-bit SHA-1 hash of the octet 0x99,
 *      followed by the two-octet packet length, followed by the entire
 *         Public-Key packet starting with the version field.  The Key ID is the
 *            low-order 64 bits of the fingerprint.  Here are the fields of the
 *               hash material, with the example of a DSA key:
 */


/* lorawan
Rate    Max payload size
0       51
1       51
2       51
3       115
4       222
5       222
6       222
7       222
8:15    not defined
*/

#ifdef __cplusplus
} /* end of extern "C" */
#endif

#endif

#define packets_h

