#include <stdarg.h>
#include <uECC.h>
#include <AESLib.h>
#include <RadioHead.h>
#include <RH_ASK.h>
#include <RHDatagram.h>
#include <SPI.h> // Not actualy used but needed to compile
#include <string.h>
#include <Wire.h>
#include <LCDI2C_LK162-12.h>
#include <MemoryFree.h>
#include <pagerlib.h>

#define NUM_ECC_DIGITS 24 //size of privkey, curvesize in bytes 
#define CURVE uECC_secp192r1()
LCDI2C lcd = LCDI2C(2,16,0x50,0);

aes_context ctx; // context for the cbc crypto stuff

#define ADDRESS 2
#define MESSAGE_SIZE 32
#define PAGER_MESSAGE_SIZE 57

//#define MESSAGE_SIZE 64
//#define PAGER_MESSAGE_SIZE 99


RH_ASK driver(2000);
RHDatagram manager(driver, ADDRESS);

const struct uECC_Curve_t * curve = CURVE;
uint8_t sharedSecret[NUM_ECC_DIGITS];

// uECC keys 192bit
uint8_t privkey[NUM_ECC_DIGITS+1] = {0xAD, 0x98, 0x8E, 0xC4, 0x79, 0x1D, 0xE0, 0x2C, 0xEE, 0xF8, 0xB0, 0xAA, 0xC9, 0x3E, 0x6F, 0x9D, 0x1E, 0x5E, 0xF7, 0x96, 0xD7, 0x3F, 0x7F, 0x2E, 0xF4};

static int RNG(uint8_t *dest, unsigned size) {
  // Use the least-significant bits from the ADC for an unconnected pin (or connected to a source of 
  // random noise). This can take a long time to generate random data if the result of analogRead(0) 
  // doesn't change very frequently.
  while (size) {
    uint8_t val = 0;
    for (unsigned i = 0; i < 8; ++i) {
      int init = analogRead(0);
      int count = 0;
      while (analogRead(0) == init) {
        ++count;
      }
      
      if (count == 0) {
         val = (val << 1) | (init & 0x01);
      } else {
         val = (val << 1) | (count & 0x01);
      }
    }
    *dest = val;
    ++dest;
    --size;
  }
  // NOTE: it would be a good idea to hash the resulting random data using SHA-256 or similar.
  return 1;
}

void generateKeys()
{
  Serial.println("generating keys");
  const struct uECC_Curve_t * curve = CURVE; 
  uint8_t private1[NUM_ECC_DIGITS+1];
  uint8_t public1[NUM_ECC_DIGITS*2];
  uint8_t secret1[NUM_ECC_DIGITS];
  
  unsigned long a = millis();
  uECC_make_key(public1, private1, curve);
  unsigned long b = millis();

  Serial.print("Generated keypair in "); Serial.print(b-a); Serial.println("Milliseconds");
  Serial.print("uint8_t pubkey[NUM_ECC_DIGITS*2] = {");
  for( int i = 0 ; i < sizeof(public1); i++){
    Serial.print("0x");
    Serial.print(public1[i], HEX);
    Serial.print(", ");
  }   
  Serial.println("}"); 
  Serial.print("uint8_t privkey[NUM_ECC_DIGITS+1] = {");
  for( int i = 0 ; i < sizeof(private1); i++){
    Serial.print("0x");
    Serial.print(private1[i], HEX);
    Serial.print(", ");
  }  
  Serial.println("}");
}

void hashSecret(uint8_t *p_secret)
{
    Serial.println("Secret:");
    for( int i=0; i < NUM_ECC_DIGITS; i++){
      Serial.print(p_secret[i]);
      Serial.print(" ");
    }
   Serial.println("");
}

char * encryptAES(char* p_data, uint8_t *p_key)
{
    uint8_t iv[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
    aes_context ctx;
    ctx = aes192_cbc_enc_start(p_key, iv);
    aes192_cbc_enc_continue(ctx, p_data, 32);
    aes192_cbc_enc_finish(ctx);
//    Serial.print("encrypted-cbc:");
//    Serial.println(p_data);   
    return p_data;
}

char* decryptAES(char* p_data, uint8_t *p_key)
{
//   Serial.println("Decrypting with key:");
//   hashSecret(sharedSecret);
   uint8_t iv[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
   aes_context ctx;
   ctx = aes192_cbc_dec_start(p_key, iv);
   aes192_cbc_dec_continue(ctx, p_data, 32);
   aes192_cbc_dec_finish(ctx);
   Serial.print("decrypted-cbc:");
   Serial.println(p_data);
   return p_data;
}

void calcSharedSecret(uint8_t *p_pubkey, uint8_t *p_privkey)
{
//    delay(100);

      if(uECC_valid_public_key(p_pubkey, curve) == 1){
//        Serial.println("Valid pubkey");
        uECC_shared_secret(p_pubkey, p_privkey, sharedSecret, curve);
//      hashSecret(sharedSecret);
    }
//    else
//     Serial.println("INVALID pubkey");
//    delay(100);
    //return sharedSecret;
}

void setup()
{
    lcd.init();
    lcd.println("dit is een pager");
    Serial.begin(9600);	// Debugging only
    Serial.println("RX init");
    if (!manager.init())
         Serial.println("init failed");
//    randomSeed(analogRead(0));
    Serial.print("freeMemory()=");
    Serial.println(freeMemory());
    uECC_set_rng(&RNG);
}

void display(char *msg)
{
/*   char line1[16];
   char line2[16];
   for(int i=0; i < MESSAGE_SIZE ; i++){
      if(i < 16)
	line1[i] = msg[i];
      else
	line2[i-16] = msg[i];
   };
*/
   lcd.clear();
   lcd.println(msg);
}

void loop()
{
//   generateKeys(); //function to generate keys, have to make a program for this to run on a computer
    // pager vars
    char *decryptedMessage;

    // crypto vars //
    uint8_t remotePubkey[NUM_ECC_DIGITS*2];
    uint8_t compressedPubkey[NUM_ECC_DIGITS+1]; 

    // Radio vars //
    uint8_t receivedData[MESSAGE_SIZE];
    uint8_t buf[RH_ASK_MAX_MESSAGE_LEN];
    uint8_t buflen = sizeof(buf);
    uint8_t from;
    uint8_t to;
    uint8_t id;   
    
    manager.waitAvailable();
    if (manager.recvfrom(buf, &buflen, &from, &to ,&id))
    {
      Serial.print("got message from : ");
      Serial.print(from, DEC);
      Serial.print(" : ");
      Serial.print(id);
      Serial.println(" : ");
      if (id == 50){ // receiving a public key...
        for (int i = 0 ; i < NUM_ECC_DIGITS+1 ; i++){
          compressedPubkey[i] = buf[i]; 	
        }
        uECC_decompress(compressedPubkey, remotePubkey, curve);
        calcSharedSecret(remotePubkey, privkey);
      } 

      if (id == 51){ // receiving an encrypted message...
        for(int i = 0 ; i < MESSAGE_SIZE; i++){
          receivedData[i] = buf[i];
        }
        decryptAES((char*)receivedData, sharedSecret);
      }

      if (id == 52){ // receiving a complete message (pubkey and message)...
        for (int i = 0 ; i < NUM_ECC_DIGITS+1 ; i++){ // first 25 byts is the compressed uECC pubkey
          compressedPubkey[i] = buf[i]; 	
        }
        uECC_decompress(compressedPubkey, remotePubkey, curve);
        calcSharedSecret(remotePubkey, privkey); // then 32 bytes of encrypted message 
        for(int i=NUM_ECC_DIGITS+1; i < NUM_ECC_DIGITS+33; i++){
          receivedData[i-(NUM_ECC_DIGITS+1)] = buf[i];
        }
        decryptedMessage = decryptAES((char*)receivedData, sharedSecret);
	display(decryptedMessage);
      } 
    }

}
